const mongoose = require("mongoose");

const mongoosePath =
  process.env.MONGOOSE_PATH || "mongodb://localhost/freecharge";

const initializeMongoConnection = () => {
  mongoose
    .connect(mongoosePath, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    .then(() => {
      console.log("Successfully connected to Database");
    })
    .catch(() => {
      console.log("Failed to connect the Database");
    });
};

module.exports = initializeMongoConnection;
