const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const initializeMongoConnection = require("./database/mongoose");
const routes = require("./routes/user");

// Configuration
dotenv.config();

// Setup connections to mongoDB
initializeMongoConnection();

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: false }));

app.use("/user", routes);

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
