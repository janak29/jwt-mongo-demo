const jwt = require("jsonwebtoken");

const secretKey = process.env.SECRET_KEY || "RANDOMSECRETKEYFORYOURAPP";

const auth = (request, response, next) => {
  const token = request.header("token");
  if (!token) {
    response.status(401).json({
      error: "Token Missing"
    });
  }

  try {
    const decoded = jwt.verify(token, secretKey);
    request.user = decoded.user;
    next();
  } catch (error) {
    response.status(401).send({ error: "Token Invalid" });
  }
};

module.exports = auth;
