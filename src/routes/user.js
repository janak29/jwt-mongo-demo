const express = require("express");
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const router = express.Router();

const User = require("../model/User");
const auth = require("../middleware/auth");

const saltRound = process.env.SALT_ROUND || 10;
const tokenExpiry = process.env.TOKEN_EXPIRY || 900;
const secretKey = process.env.SECRET_KEY || "RANDOMSECRETKEYFORYOURAPP";

router.post(
  "/register",
  [
    check("username", "Please Enter a valid Username")
      .not()
      .isEmpty(),
    check("email", "Please enter a valid Email").isEmail(),
    check("password", "Please enter a valid Password").isLength({ min: 6 })
  ],
  async (request, response) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      response.status(400).json({
        errors: errors.array()
      });
    }

    const { username, email, password } = request.body;
    try {
      let user = await User.findOne({ email });
      if (user) {
        response.status(400).json({
          error: "User already exists"
        });
      }

      user = new User({ username, email, password });

      const salt = await bcrypt.genSalt(saltRound);
      user.password = await bcrypt.hash(password, salt);

      await user.save();

      const payload = { user: { id: user.id } };

      jwt.sign(
        payload,
        secretKey,
        { expiresIn: tokenExpiry },
        (error, token) => {
          if (error) throw error;

          const currentTimestamp = new Date();
          currentTimestamp.setMinutes(currentTimestamp.getMinutes() + 15);

          response.status(200).json({
            token,
            exp: currentTimestamp
          });
        }
      );
    } catch (error) {
      response
        .status(400)
        .send({ error: "Error in Saving User Registration Details" });
    }
  }
);

router.post(
  "/login",
  [
    check("email", "Please enter a valid Email").isEmail(),
    check("password", "Please enter a valid Password").isLength({ min: 6 })
  ],
  async (request, response) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      response.status(400).json({
        errors: errors.array()
      });
    }

    const { email, password } = request.body;

    try {
      const user = await User.findOne({ email });

      if (!user) {
        response.status(400).json({
          error: "User doesn't exists"
        });
      }

      const passwordCheck = await bcrypt.compare(password, user.password);

      if (!passwordCheck) {
        response.status(400).json({
          error: "Password doesn't match"
        });
      }
      const payload = { user: { id: user.id } };

      jwt.sign(
        payload,
        secretKey,
        { expiresIn: tokenExpiry },
        (error, token) => {
          if (error) throw error;

          const currentTimestamp = new Date();
          currentTimestamp.setMinutes(currentTimestamp.getMinutes() + 15);

          response.status(200).json({
            token,
            exp: currentTimestamp
          });
        }
      );
    } catch (error) {
      response.status(400).send({ error: "Error in Login" });
    }
  }
);

router.get("/me", auth, async (request, response) => {
  try {
    const user = await User.findById(request.user.id);
    response.status(200).json(user);
  } catch (error) {
    response.status(400).send({ error: "Error in Fetching user" });
  }
});

router.get("/verify-token", async (request, response) => {
  const token = request.header("token");
  if (!token) {
    response.status(401).json({
      error: "Token Missing"
    });
  }

  try {
    jwt.verify(token, secretKey);
    response.status(200).json({
      token
    });
  } catch (error) {
    if (error.name === "TokenExpiredError") {
      const userId = jwt.decode(token, secretKey);

      const payload = { user: { id: userId.user.id } };

      jwt.sign(
        payload,
        secretKey,
        { expiresIn: tokenExpiry },
        (jwterror, jwttoken) => {
          if (jwterror) throw jwterror;

          const currentTimestamp = new Date();
          currentTimestamp.setMinutes(currentTimestamp.getMinutes() + 15);

          response.status(200).json({
            jwttoken,
            exp: currentTimestamp
          });
        }
      );
    } else {
      response.status(401).send({ error: "Token Invalid" });
    }
  }
});

router.all("*", (request, response) => {
  response.status(404).send({ error: "This URL is not available" });
});

module.exports = router;
