# JWT Nodejs Mongodb

This project uses <code>Nodejs</code> as backend, <code>Mongodb</code> as database and <code>JWT</code> for authorization.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development.

### Prerequisites

Below are the few softwares that needs to be installed

>Nodejs <br/>
>Mongodb <br/>

### Installing

A step by step series of examples that tell you how to get a development env running

>1. Clone the project using `git clone https://gitlab.com/janak29/jwt-mongo-demo.git` <br/>
>2. Move to the working directory <code>cd jwt-mongo-demo</code> <br/>
>3. Install the node modules by using <code>npm install</code> <br/>
>4. Update the .env file, if needed otherwise you are ready to launch the application. <br/>

### A typical top-level directory layout
    .
    ├── node_modules            # Libraries that are used in the application
    ├── src                     # Source files
    ├    ├── database           # Database Connection
    ├    ├── middleware         # Middleware checks for the authorization token
    ├    ├── model              # Database Schema
    ├    ├── routes             # Routes used within the application
    ├    ├── server.js          # Entry point to the application
    ├── package.json            # List of packages needed to start the application
    └── README.md               # Breif summary of the application

### Running

Use the below command to run the application in hot reload mode

>npm run dev-serve <br/>

### Styling

Use the below command to format and lint the code.

>npm run lint <br/>

#### Built With

* [ESlint](https://github.com/eslint/eslint)
* [Prettier](https://github.com/prettier/prettier)
* [Airbnb Guide](https://github.com/airbnb/javascript)

### Go Live

Use the PM2 to run the application in Production mode, for that install [PM2](https://www.npmjs.com/package/pm2)

>npm run prod-serve <br/>
